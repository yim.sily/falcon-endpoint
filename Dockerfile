FROM ubuntu:latest

#节点唯一编码
ENV UUID ''
#mysql主机host
ENV MYSQL_HOST ''
#mysql端口
ENV MYSQL_PORT ''
#数据库
ENV MYSQL_DATABASE ''
#用户名
ENV MYSQL_USER ''
#用户密码
ENV MYSQL_PASSWORD ''
#数据库编码 (非必须，默认utf8)
ENV MYSQL_CHARSET 'utf8'


#安装基础软件
RUN apt-get update && apt-get install wget curl \
gcc g++ make perl libpcre3 libpcre3-dev openssl libssl-dev zlib1g-dev git -y && apt-get autoremove -y

#安装nginx
RUN wget -q 'https://openresty.org/download/openresty-1.21.4.1.tar.gz' && \ 
tar -zxf openresty-1.21.4.1.tar.gz && rm -f openresty-1.21.4.1.tar.gz && cd openresty-1.21.4.1/ && \ 
./configure --prefix=/ && make && make install && cd ../ && rm -rf openresty-1.21.4.1 
COPY nginx.conf /nginx/conf/nginx.conf
COPY index.html /nginx/html/index.html
COPY icon.png /nginx/html/icon.png
ADD scripts/ /scripts

#安装科学工具
COPY entrypoint.sh /usr/local/falcon-server/
COPY config.json /usr/local/falcon-server/ 

RUN git clone https://gitlab.com/yim.sily/falcon-common.git && cp falcon-common/* /usr/local/falcon-server/ -rf && rm falcon-common -rf && \
chmod a+x /usr/local/falcon-server/entrypoint.sh && \
chmod a+x /usr/local/falcon-server/falcon-x86_64 && \
chmod a+x /usr/local/falcon-server/falcon-aarch64

EXPOSE 80
USER root
     
ENTRYPOINT [ "/usr/local/falcon-server/entrypoint.sh" ]
