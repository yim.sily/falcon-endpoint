#!/bin/sh

sed -i "s#UUID#$UUID#g" /usr/local/falcon-server/config.json

mv "/usr/local/falcon-server/falcon-`arch`" /usr/local/falcon-server/falcon

nohup /usr/local/falcon-server/falcon run -c /usr/local/falcon-server/config.json >> /dev/stdout 2>&1 &

/nginx/sbin/nginx -g 'daemon off;'